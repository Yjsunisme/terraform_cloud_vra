provider vra {
  url           = var.vra_url
  refresh_token = var.vra_refresh_token
}

resource "vra_cloud_account_aws" "this" {
  name        = "AWS Cloud Account"
  description = "AWS Cloud Account configured by Terraform"
  access_key  = var.aws_access_key
  secret_key  = var.aws_secret_key
  regions     = ["us-east-1", "us-west-1"]

  tags {
    key   = "cloud"
    value = "aws"
  }
}

data "vra_region" "this" {
  cloud_account_id = vra_cloud_account_aws.this.id
  region           = "us-west-1"
}

resource "vra_project" "this" {
   name      = "test-project"
   region_id = data.vra_region.this.id
}